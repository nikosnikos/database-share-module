<?php

/**
 * TOTALEMENT OBSOLETE MAIS LAISSE ICI PARCE QUE LA VUE https://database.shareimpro.eu/fr/admin/structure/views/view/search A ENCORE LE PLUGIN EN MEMOIRE
 * Utiliser maintenant le style leaflet map. 
 * 
 * Ce style de view avait été créé parce qu'à l'époque on ne pouvait pas mixer plusieurs map dedans. Typiquement pour nous il s'agissait de mixer les contenus et les users.
 */
namespace Drupal\dbshare\Plugin\views\style;

use Drupal\leaflet_views\Plugin\views\style\LeafletMap;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\leaflet_views\Controller\LeafletAjaxPopupController;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Entity\Index;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\ViewExecutable;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Leaflet\LeafletService;
use Drupal\Component\Utility\Html;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\leaflet\LeafletSettingsElementsTrait;
use Drupal\views\Plugin\views\PluginBase;
use Drupal\views\Views;

/**
 * Style plugin to render a View output as a Leaflet map.
 *
 * @ingroup views_style_plugins
 *
 * Attributes set below end up in the $this->definition[] array.
 *
 * @ViewsStyle(
 *   id = "leaflet_map_dbshare",
 *   title = @Translation("DBShare Leaflet Map"),
 *   help = @Translation("Displays a View as a Leaflet map."),
 *   display_types = {"normal"},
 *   theme = "leaflet-map"
 * )
 */
class DBShareLeafletMap extends LeafletMap implements ContainerFactoryPluginInterface {

  use LeafletSettingsElementsTrait;

  /**
   * The Default Settings.
   *
   * @var array
   */
  protected $defaultSettings;

  /**
   * The Entity source property.
   *
   * @var string
   */
  protected $entitySource;

  /**
   * The Entity type property.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The Entity Info service property.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityInfo;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * The Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The Entity Field manager service property.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Entity Display Repository service property.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplay;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;


  /**
   * The Renderer service property.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $renderer;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Leaflet service.
   *
   * @var \Drupal\Leaflet\LeafletService
   */
  protected $leafletService;

  /**
   * The Link generator Service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $link;

  /**
   * The list of fields added to the view.
   *
   * @var array
   */
  protected $viewFields = [];

  /**
   * Field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * Constructs a LeafletMap style instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display
   *   The entity display manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Leaflet\LeafletService $leaflet_service
   *   The Leaflet service.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The Link Generator service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityDisplayRepositoryInterface $entity_display,
    AccountInterface $current_user,
    MessengerInterface $messenger,
    RendererInterface $renderer,
    ModuleHandlerInterface $module_handler,
    LeafletService $leaflet_service,
    LinkGeneratorInterface $link_generator,
    FieldTypePluginManagerInterface $field_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_manager, $entity_field_manager, $entity_display, $current_user, $messenger, $renderer, $module_handler, $leaflet_service, $link_generator, $field_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository'),
      $container->get('current_user'),
      $container->get('messenger'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('leaflet.service'),
      $container->get('link_generator'),
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    // If data source changed then apply the changes.
    if ($form_state->get('entity_source')) {
      $this->options['entity_source'] = $form_state->get('entity_source');
      $this->entityInfo = $this->getEntitySourceEntityInfo($this->options['entity_source']);
      $this->entityType = $this->entityInfo->id();
      $this->entitySource = $this->options['entity_source'];
    }

    \Drupal::messenger()->addMessage(t('!!! This is a totaly custom style views plugin inherited from LeafleftMap module !!! It only works if field_geofield and field_geofield_2 is set as field and is geofield.'), 'warning');

    parent::buildOptionsForm($form, $form_state);

    unset($form['data_source']);
  }

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    unset($options['data_source']);
    return $options;
  }



  /**
   * Renders the View.
   */
  public function render() {
    $data = [];

    // Collect bubbleable metadata when doing early rendering.
    $build_for_bubbleable_metadata = [];

    // Always render the map, otherwise ...
    $leaflet_map_style = !isset($this->options['leaflet_map']) ? $this->options['map'] : $this->options['leaflet_map'];
    $map = leaflet_map_get_info($leaflet_map_style);

    // Set Map additional map Settings.
    $this->setAdditionalMapOptions($map, $this->options);

    // Add a specific map id.
    $map['id'] = Html::getUniqueId("leaflet_map_view_" . $this->view->id() . '_' . $this->view->current_display);
    $this->renderFields($this->view->result);

    /* @var \Drupal\views\ResultRow $result */
    foreach ($this->view->result as $id => $result) {

      // For proper processing make sure the geofield_value is created as
      // an array, also if single value.
      // XXX WARNING We take the value of an hard coded field name and don't check if it exsists !!!
      $geofield_name = 'field_geofield';
      $geofield_value = (array) $this->getFieldValue($result->index, $geofield_name);
      if (empty($geofield_value)) {
        $geofield_name = 'field_geofield_1';
        $geofield_value = (array) $this->getFieldValue($result->index, $geofield_name);
      }
      if (!empty($geofield_value)) {

        $features = $this->leafletService->leafletProcessGeofield($geofield_value);

        if (!empty($result->_entity)) {
          // Entity API provides a plain entity object.
          $entity = $result->_entity;
        }
        elseif (isset($result->_object)) {
          // Search API provides a TypedData EntityAdapter.
          $entity_adapter = $result->_object;
          if ($entity_adapter instanceof EntityAdapter) {
            $entity = $entity_adapter->getValue();
          }
        }

        // Render the entity with the selected view mode.
        if (isset($entity)) {
          // Get and set (if not set) the Geofield cardinality.
          /* @var \Drupal\Core\Field\FieldItemList $geofield_entity */
          if (!isset($map['geofield_cardinality'])) {
            try {
              $geofield_entity = $entity->get($geofield_name);
              $map['geofield_cardinality'] = $geofield_entity->getFieldDefinition()
                ->getFieldStorageDefinition()
                ->getCardinality();
            }
            catch (\Exception $e) {
              // In case of exception it means that $geofield_name field is
              // not directly related to the $entity and might be the case of
              // a geofield exposed through a relationship.
              // In this case it is too complicate to get the geofield related
              // entity, so apply a more general case of multiple/infinite
              // geofield_cardinality.
              // @see: https://www.drupal.org/project/leaflet/issues/3048089
              $map['geofield_cardinality'] = -1;
            }
          }

          $entity_type = $entity->getEntityTypeId();
          $entity_type_langcode_attribute = $entity_type . '_field_data_langcode';

          $view = $this->view;

          // Set the langcode to be used for rendering the entity.
          $rendering_language = $view->display_handler->getOption('rendering_language');
          $dynamic_renderers = [
            '***LANGUAGE_entity_translation***' => 'TranslationLanguageRenderer',
            '***LANGUAGE_entity_default***' => 'DefaultLanguageRenderer',
          ];
          if (isset($dynamic_renderers[$rendering_language])) {
            /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
            $langcode = isset($result->$entity_type_langcode_attribute) ? $result->$entity_type_langcode_attribute : $entity->language()
              ->getId();
          }
          else {
            if (strpos($rendering_language, '***LANGUAGE_') !== FALSE) {
              $langcode = PluginBase::queryLanguageSubstitutions()[$rendering_language];
            }
            else {
              // Specific langcode set.
              $langcode = $rendering_language;
            }
          }

          switch ($this->options['description_field']) {
            case '#rendered_entity':
              $build = $this->entityManager->getViewBuilder($entity->getEntityTypeId())
                ->view($entity, $this->options['view_mode'], $langcode);
              $render_context = new RenderContext();
              $description = $this->renderer->executeInRenderContext($render_context, function () use (&$build) {
                return $this->renderer->render($build, TRUE);
              });
              if (!$render_context->isEmpty()) {
                $render_context->update($build_for_bubbleable_metadata);
              }
              break;

            case '#rendered_entity_ajax':
              $parameters = [
                'entity_type' => $entity_type,
                'entity' => $entity->id(),
                'view_mode' => $this->options['view_mode'],
                'langcode' => $langcode,
              ];
              $url = Url::fromRoute('leaflet_views.ajax_popup', $parameters);
              $description = sprintf('<div class="leaflet-ajax-popup" data-leaflet-ajax-popup="%s" %s></div>',
                $url->toString(), LeafletAjaxPopupController::getPopupIdentifierAttribute($entity_type, $entity->id(), $this->options['view_mode'], $langcode));
              $map['settings']['ajaxPoup'] = TRUE;
              break;

            case '#rendered_view_fields':
              // Normal rendering via view/row fields (with labels options,
              // formatters, classes, etc.).
              $render_row = [
                "markup" => $this->view->rowPlugin->render($result),
              ];
              $description = !empty($this->options['description_field']) ? $this->renderer->renderPlain($render_row) : '';
              break;

            default:
              // Row rendering of single specified field value (without
              // labels).
              $description = !empty($this->options['description_field']) ? $this->rendered_fields[$result->index][$this->options['description_field']] : '';
          }

          // Merge eventual map icon definition from hook_leaflet_map_info.
          if (!empty($map['icon'])) {
            $this->options['icon'] = $this->options['icon'] ?: [];

            // Remove empty icon options so that they might be replaced by
            // the ones set by the hook_leaflet_map_info.
            foreach ($this->options['icon'] as $k => $icon_option) {
              if (empty($icon_option) || (is_array($icon_option) && $this->leafletService->multipleEmpty($icon_option))) {
                unset($this->options['icon'][$k]);
              }
            }
            $this->options['icon'] = array_replace($map['icon'], $this->options['icon']);
          }

          // Define possible tokens.
          $tokens = [];
          foreach ($this->rendered_fields[$result->index] as $field_name => $field_value) {
            $tokens[$field_name] = $field_value;
            $tokens["{{ $field_name }}"] = $field_value;
          }

          $icon_type = isset($this->options['icon']['iconType']) ? $this->options['icon']['iconType'] : 'marker';

          // Relates the feature with additional properties.
          foreach ($features as &$feature) {

            // Attach pop-ups if we have a description field.
            // Add its entity id, so that it might be referenced from outside.
            $feature['entity_id'] = $entity->id();

            // Generate the weight feature property
            // (falls back to natural result ordering).
            $feature['weight'] = !empty($this->options['weight']) ? intval(str_replace(["\n", "\r"], "", $this->viewsTokenReplace($this->options['weight'], $tokens))) : $id;

            // Attach pop-ups if we have a description field.
            if (isset($description)) {
              $feature['popup'] = $description;
            }
            // Attach also titles, they might be used later on.
            if ($this->options['name_field']) {
              // Decode any entities because JS will encode them again and
              // we don't want double encoding.
              $feature['label'] = !empty($this->options['name_field']) ? Html::decodeEntities(($this->rendered_fields[$result->index][$this->options['name_field']])) : '';
            }

            // Eventually set the custom Marker icon (DivIcon, Icon Url or
            // Circle Marker).
            if ($feature['type'] === 'point' && isset($this->options['icon'])) {
              // Set Feature Icon properties.
              $feature['icon'] = $this->options['icon'];

              // Transforms Icon Options that support Replacement
              // Patterns/Tokens.
              if (!empty($this->options["icon"]["iconSize"]["x"])) {
                $feature['icon']["iconSize"]["x"] = $this->viewsTokenReplace($this->options["icon"]["iconSize"]["x"], $tokens);
              }
              if (!empty($this->options["icon"]["iconSize"]["y"])) {
                $feature['icon']["iconSize"]["y"] = $this->viewsTokenReplace($this->options["icon"]["iconSize"]["y"], $tokens);
              }
              if (!empty($this->options["icon"]["shadowSize"]["x"])) {
                $feature['icon']["shadowSize"]["x"] = $this->viewsTokenReplace($this->options["icon"]["shadowSize"]["x"], $tokens);
              }
              if (!empty($this->options["icon"]["shadowSize"]["y"])) {
                $feature['icon']["shadowSize"]["y"] = $this->viewsTokenReplace($this->options["icon"]["shadowSize"]["y"], $tokens);
              }

              switch ($icon_type) {
                case 'html':
                  $feature['icon']['html'] = str_replace(["\n", "\r"], "", $this->viewsTokenReplace($this->options['icon']['html'], $tokens));
                  $feature['icon']['html_class'] = $this->options['icon']['html_class'];
                  break;

                case 'circle_marker':
                  $feature['icon']['options'] = str_replace(["\n", "\r"], "", $this->viewsTokenReplace($this->options['icon']['circle_marker_options'], $tokens));
                  break;

                default:
                  if (!empty($this->options['icon']['iconUrl'])) {
                    $feature['icon']['iconUrl'] = str_replace(["\n", "\r"], "", $this->viewsTokenReplace($this->options['icon']['iconUrl'], $tokens));
                    // Generate correct Absolute iconUrl & shadowUrl,
                    // if not external.
                    if (!empty($feature['icon']['iconUrl'])) {
                      $feature['icon']['iconUrl'] = $this->leafletService->generateAbsoluteString($feature['icon']['iconUrl']);
                    }
                  }
                  if (!empty($this->options['icon']['shadowUrl'])) {
                    $feature['icon']['shadowUrl'] = str_replace(["\n", "\r"], "", $this->viewsTokenReplace($this->options['icon']['shadowUrl'], $tokens));
                    if (!empty($feature['icon']['shadowUrl'])) {
                      $feature['icon']['shadowUrl'] = $this->leafletService->generateAbsoluteString($feature['icon']['shadowUrl']);
                    }
                  }

                  // Set the Feature IconSize and ShadowSize to the IconUrl or
                  // ShadowUrl Image sizes (if empty or invalid).
                  $this->leafletService->setFeatureIconSizesIfEmptyOrInvalid($feature);

                  break;
              }
            }

            // Associate dynamic path properties (token based) to each
            // feature, in case of not point.
            if ($feature['type'] !== 'point') {
              $feature['path'] = str_replace(["\n", "\r"], "", $this->viewsTokenReplace($this->options['path'], $tokens));
            }

            // Associate dynamic className property (token based) to icon.
            $feature['icon']['className'] = !empty($this->options['icon']['className']) ? str_replace(["\n", "\r"], "", $this->viewsTokenReplace($this->options['icon']['className'], $tokens)) : '';

            // Allow modules to adjust the marker.
            $this->moduleHandler->alter('leaflet_views_feature', $feature, $result, $this->view->rowPlugin);
          }

          // Add new points to the whole basket.
          $data = array_merge($data, $features);
        }
      }
    }

    // Don't render the map, if we do not have any data
    // and the hide option is set.
    if (empty($data) && !empty($this->options['hide_empty_map'])) {
      return [];
    }

    // Order the data features based on the 'weight' element.
    uasort($data, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    $js_settings = [
      'map' => $map,
      'features' => $data,
    ];

    // Allow other modules to add/alter the map js settings.
    $this->moduleHandler->alter('leaflet_map_view_style', $js_settings, $this);

    $map_height = !empty($this->options['height']) ? $this->options['height'] . $this->options['height_unit'] : '';
    $element = $this->leafletService->leafletRenderMap($js_settings['map'], $js_settings['features'], $map_height);
    // Add the Core Drupal Ajax library for Ajax Popups.
    if (isset($map['settings']['ajaxPoup']) && $map['settings']['ajaxPoup'] == TRUE) {
      $build_for_bubbleable_metadata['#attached']['library'][] = 'core/drupal.ajax';
    }
    BubbleableMetadata::createFromRenderArray($element)
      ->merge(BubbleableMetadata::createFromRenderArray($build_for_bubbleable_metadata))
      ->applyTo($element);
    return $element;
  }
}
