<?php
namespace Drupal\dbshare\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the Example module.
 */
class ExportsController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function page() {
    return [
      '#markup' => "<p>Cette page permet d'exporter les différents types de données du site au format CSV.</p> <p>Pour cela, aller dans l'onglet correspondant au contenu à exporter, et tout en bas à gauche cliquer sur le bouton orange CSV.</p>",
    ];
  }

}
