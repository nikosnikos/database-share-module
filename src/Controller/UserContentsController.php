<?php

namespace Drupal\dbshare\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Contents list the user can edit
 */
class UserContentsController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The database replica connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $databaseReplica;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a UserContentsController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Database\Connection $databaseReplica
   *   The database replica connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(Connection $database, Connection $databaseReplica, DateFormatterInterface $dateFormatter, EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->databaseReplica = $databaseReplica;
    $this->dateFormatter = $dateFormatter;
    $this->entityTypeManager = $entityTypeManager;
    $this->nodeStorage = $entityTypeManager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('database.replica'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Title callback for the tracker.user_tab route.
   *
   * @return string
   *   The title.
   */
  public function getTitle() {
    return $this->currentUser()->getDisplayName();
  }

  /**
   * Builds content for the contents user can edit controllers.
   *
   * @return array
   *   The render array.
   */
  public function buildContent() {
    // Select nid user own or have access with ACL module.
    $editable_nodes = $this->database
      ->query("SELECT an.nid FROM {acl_node} an INNER JOIN {acl_user} au ON an.acl_id = au.acl_id WHERE an.grant_update = 1 AND au.uid = :uid UNION SELECT nid FROM {node_field_data} WHERE uid = :uid", [
      'uid' => $this->currentUser()->id(),
    ])->fetchCol();

    $cacheable_metadata = new CacheableMetadata();
    $rows = [];
    if (!empty($editable_nodes)) {
      /** @var \Drupal\node\NodeInterface[] $nodes */
      $nodes = $this->nodeStorage->loadMultiple($editable_nodes);
      // Display the data.
      foreach ($nodes as $node) {
        $owner = $node->getOwner();
        $row = [
          'type' => node_get_type_label($node),
          'title' => [
            'data' => [
              '#type' => 'link',
              '#url' => $node->toUrl(),
              '#title' => $node->getTitle() . (! $node->isPublished() ? t(' (Draft)') : ''),
            ],
            'data-history-node-id' => $node->id(),
            'data-history-node-timestamp' => $node->getChangedTime(),
          ],
          'last updated' => [
            'data' => t('@time ago', [
              '@time' => $this->dateFormatter->formatTimeDiffSince($node->getChangedTime()),
            ]),
          ],
        ];

        $rows[] = $row;

        // Add node to cache tags.
        $cacheable_metadata->addCacheTags($node->getCacheTags());
      }
    }

    // Add the list cache tag for nodes.
    $cacheable_metadata->addCacheTags($this->nodeStorage->getEntityType()->getListCacheTags());

    $page['#prefix'] = '<div class="contents-user-list">';
    $page['#suffix'] = '</div>';
    $page['intro'] = [
      '#markup' => '<h1>'.t('Contents I can edit').'</h1><p>'.t('This is the list of all the contents you can edit.').'</p>',
    ];
    $page['content_list'] = [
      '#rows' => $rows,
      '#header' => [
        $this->t('Type'),
        $this->t('Title'),
        $this->t('Last updated'),
      ],
      '#type' => 'table',
      '#empty' => $this->t('No content available.'),
    ];
    $page['pager'] = [
      '#type' => 'pager',
      '#weight' => 10,
    ];
    $page['#sorted'] = TRUE;
    $cacheable_metadata->addCacheContexts(['user.node_grants:view']);

    $cacheable_metadata->applyTo($page);
    return $page;
  }

}
